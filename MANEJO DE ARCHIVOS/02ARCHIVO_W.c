// P.D.E INSTITUTO SAN JOSE A-355
// ING. GABRIEL GARCIA
// EJERCICIO 02ARCHIVO_W

#include <stdio.h>

/*Escribir en el archivo de texto

    fputc() escribe un car�cter en el archivo, recibe dos par�metros, el car�cter a escribir y la variable de archivo.
    fputs() escribe una cadena en el archivo, recibe dos par�metros, la cadena a escribir, y la variable de archivo.
    fprintf() funciona de la misma forma que printf() pero su primer par�metro es la variable de archivo.*/

int main(){
  // La variable de archivo
  FILE *PunteroAlArchivo;
  // Variable para escribir datos en el archivo
    char UnaCadena[100] = "\nEjercicios de Manejo de Archivos";
    char letra;

  PunteroAlArchivo = fopen("./02ARCHIVO_W.txt", "a+");
  /*Se intenta abrir el archivo llamado archivo.txt en modo Append+*/

  // Si no podemos abrir el archivo, terminamos el programa
  if(PunteroAlArchivo == NULL) { printf("No se pudo abrir el archivo... \n"); return -1; }


  fputc('E', PunteroAlArchivo);
  fputc('L', PunteroAlArchivo);
  fputc('C', PunteroAlArchivo);
  fputc('A', PunteroAlArchivo);
  fputc('\n', PunteroAlArchivo);
  // La primera linea del archivo fue escrita y contiene la frase ELCA\n

  // A Continuaci�n se ingresa una cadena
  fputs(UnaCadena, PunteroAlArchivo);

  // Ahora le toca el turno a fprintf
  fprintf(PunteroAlArchivo,"\nP.D.E INSTITUTO SAN JOSE A-355");

  //Ahora muestro en Pantalla, el contenido del Archivo
    rewind( PunteroAlArchivo ); //Sit�a el puntero al principio del archivo

  	printf( "Contenido del fichero:\n" );
	letra = getc(PunteroAlArchivo);
	while (feof(PunteroAlArchivo) == 0) { //End of File
		printf( "%c",letra );
		letra = getc(PunteroAlArchivo);
	}

  fclose(PunteroAlArchivo);

  return 0;
}
