/*

Ejercicio #3: Crear un cronometro o alarma dandole al usuario la opcion cuando frenar.

\n

*/

#include <stdio.h>
#include <windows.h> // SLEEP()

int main()
{
	// EL USUARIO DIGITARA EN QUE HORA,MINUTO,SEGUNDO
	int tipo;
	printf("1. Horas");
	printf("\n2. Minutos");
	printf("\n3. Segundos");
	printf("\nDigite su opcion cuando va a querer frenar su cronometro: ");
	scanf("%i", &tipo);

	// EL USUARIO DIGITARA LA CANTIDAD DE HORAS MINUTOS O SEGUNDOS
	int cantidad;
	printf("\nDigite la cantidad: ");
	scanf("%i" , &cantidad);

	// CREAMOS LAS VARIABLES PARA LAS HORAS , MINUTOS , SEGUNDOS
	int hora,minuto,segundo;


	// 3 BUCLES ANIDADOS DE TIPO FOR , 3 = HORAS ,MINUTOS,SEGUNDOS
	for (hora=0; hora<=24; hora++ )
	{
		for (minuto=0; minuto<60; minuto++ )

	{
		for (segundo=0; segundo<60; segundo++ )
		{
			// AGREGAMOS UN INTERVALO DE 1000 MS = 1 SEGUNDO
			Sleep(1000);


			// IMPRIMIR NUESTRO CRONOMETRO
			printf("\r %.2i : %.2i : %i " , hora , minuto , segundo );

			// CONDICIONAL PARA LAS HORAS
			if (tipo == 1 && cantidad <= hora)
			{
				printf("\nCronometro finalizado");
				return 0;
			}

			// CONDICIONAL PARA LOS MINUTOS
			else if (tipo == 2 && cantidad <= minuto)
			{
				printf("\nCronometro finalizado");
				return 0;
			}
			// CONDICIONAL PARA LOS SEGUNDOS
			else if (tipo == 3 && cantidad <= segundo)
			{
				printf("\nCronometro finalizado");
				return 0;
			}



		}
		}
		}



	return 0;
}
